This directory contains the Swedish files frol the 1.3 edition with the empty nodes deleted "brutally" (i.e. without updating the "DEPS" column.
This version is needd to retrain the Seen2Seen system, which did not foresee empty nodes (with identifiers like 15.1) to be part of the format. 
All the annotated VMWEs which concerned empty nodes were manually checked and updated.
Done by Agata Savary, 31 Jan 2023.


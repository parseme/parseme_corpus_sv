README
------
This is the README file for the PARSEME verbal multiword expressions (VMWEs) corpus for Swedish, edition 1.3 (more annotations added after the Parseme shared task 1.2). See the wiki pages of the [PARSEME corpora](https://gitlab.com/parseme/corpora/-/wikis/) initiative for the full documentation of the annotation principles.

The present Swedish data are annotated from scratch based on the guidelines for version 1.2 of the PARSEME shared task.


Source corpora
-------
All the annotated data come from the Swedish-Talbanken corpus, as distributed by the [Universal dependencies project](https://universaldependencies.org/). The annotation covers all of Talbanken, 5923 sentences (504 sentences from the sv_talbanken_ud_dev original file, 1219 sentences from sv_talbanken_ud_test, and  4400 sentences from sv_talbanken_ud_train). The domain of the sentences are news texts and non-fiction (including high-school essays).
Originally, the train/dev/test splits is the same as in the UD Talbanken - those can be accessed in the [Swedish repository](https://gitlab.com/parseme/parseme_corpus_sv). In official releases (e.g. 1.3) the data are resplit by controlling for the number of unseen VMWE.


Format
--------------------

The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. The following tagsets are used:
* column 3 (lemma): available,
* column 4 (UPOS): [UD POS-tags](http://universaldependencies.org/u/pos) version 2.10, 
* column 5 (XPOS): [SUC tagset](https://cl.lingfil.uu.se/~nivre/swedish_treebank/pos.html) The language-specific tags (including features) follow the guidelines of the Stockholm-Umeå Corpus.
* column 6 (FEATS): [UD features](http://universaldependencies.org/u/feat/index.html) version 2.10
* column 8 (DEPREL): [UD dependency relations](http://universaldependencies.org/u/dep) version 2.10 
* column 11 (PARSEME:MWE): [PARSEME VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.3/?page=030_Categories_of_VMWEs) version 1.3

All UD annotations are originally manually annotated, then converted to UD, and manually checked.

The VMWE annotation was performed by a team of 6 annotators. Each file from the dec and train options is annotated by one annotator, followed by consistency checking across files. All files form the test portion was initially annotated by two annotators, followed by consolidation and consistentcy checking.

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 

Authors
----------
The VMWEs annotations (column 11) were performed by  Elsa Erenmalm, Gustav Finnveden, Bernadeta Griciūtė, Ellinor Lindqvist, Eva Pettersson and Sara Stymne (Language leader).
Consistency checking was performed by Sara Stymne.

License
----------
The VMWEs annotations (column 11) are distributed under the terms of the [CC-BY v4](https://creativecommons.org/licenses/by/4.0/) license.
Swedish-Talbanken, with the annotations in all columns except 11, is realeased under the terms of the [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license.


Contact
----------
*  sara.stymne@lingfil.uu.se

Change log
----------
- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - Changes with respect to the 1.2 version are the following:
    - more texts have been annotated
    - the morphosyntactic annotations have been updated wrt. to the UD Talbanken corpus v 2.10 by Agata Savary 
- **2020-07-09**:
  - [Version 1.2](http://hdl.handle.net/11234/1-3367) of the corpus was released on LINDAT.
  - The Swedish sample contains 1991 annotated VMWEs.
  - The Swedish data come from the Talbanken UD corpus and have manual annotations for morphosyntax (UD v 2.5).
  
- **2017-01-20**:
  - [Version 1.0](http://hdl.handle.net/11372/LRT-2282) of the corpus was released on LINDAT. 
  - The Swedish sample contains 292 annotated VMWEs and has automatic annotations for morphosyntax.
     

